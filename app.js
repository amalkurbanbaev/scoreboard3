const cheerio = require('cheerio');
const request = require('request');
const io = require('socket.io-client');
const HTTPS = require("https");


let defaultAgent = new HTTPS.Agent();

request('https://www.hltv.org/matches/2341026/liquid-vs-furia-dreamhack-masters-spring-2020-north-america', (error, response, html) => {
  if (!error && response.statusCode === 200) {
    const $ = cheerio.load(html);

    let url = $('#scoreboardElement')
      .attr('data-scorebot-url')
      .split(',')
      .pop();
    let matchId = $('#scoreboardElement').attr('data-scorebot-id');

    let socket = io.connect(url, {
      agent: defaultAgent,
      path: '/socket.io',
      transport: ['websocket']
    });
    let initObject = JSON.stringify({
      token: '',
      listId: matchId
    });

    socket.on('connect', function () {
      let done = () => {
        return socket.close();
      };

      socket.on('ping', (data) => {
        console.log(data + 'this connected');
      });

      socket.emit('ping', initObject);
    });
  }
});